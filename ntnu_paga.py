#!/usr/bin/env python3

# Author : Peter Davidse <peter.davidse@ntnu.no>
# Date   : 10.04.2017
#        : 13.02.2018 modified for use as anonymizer
# Info   : Module used for ntnu_paga file parsing


import csv
import sys
import os
import random
import datetime
import unicodedata
import ntnu_config

from collections import namedtuple

config = ntnu_config.configuration()


# USU files handled by the script
file_in = config.EXTERNAL_FILES_DIR + 'Utvidet_Serviceuttrekk_NTNU.sdv'
file_out = config.EXTERNAL_FILES_DIR + 'Anonymized_USU.sdv'

# set to help generate unique usernames
usernames = set()

# delete file_out if it exists
try:
    os.remove(file_out)
except OSError:
    pass


def file_to_list(filename,
                 delimiter=';',
                 ):
    """ create a list object from a csv file """
    try:
        with open(filename,
                  encoding=config.ENCODING,
                  ) as f:
            csv_object = csv.reader(f,
                                    delimiter=delimiter,
                                    )

            # create a list with all lines from the import file, using namedtuple
            result = [row for row in map(namedtuple('Fields',
                                                    config.USU_FIELDS)._make,
                                         csv_object,
                                         )]
    except FileNotFoundError as error:
        print(error)
        sys.exit(3)
    return result


def write_record(record):
    with open(file_out,
              'a',
              newline='',
              ) as csv_file_out:
        csv_writer = csv.writer(csv_file_out,
                                delimiter=config.OUTFILE_DELIMITER,
                                quoting=eval(config.OUTFILE_QUOTE),
                                )

        if csv_file_out.tell() == 0 and config.OUTFILE_WRITEHEADER:
            csv_writer.writerow(config.USU_FIELDS)

        csv_writer.writerow(record)


def generate_username(firstname):
    ''' return unique username '''

    firstname = name = unicodedata.normalize('NFD',
                                             firstname,
                                             ).encode('ascii',
                                                      'ignore',
                                                      ).decode(config.ENCODING).upper()

    for number in range(1,
                        1000,
                        ):

        if name not in usernames:
            usernames.add(name)
            return name
        else:
            name = firstname + str(number)


def main():
    records = file_to_list(file_in)
    firstnames = [record.FORNAVN.split()[0].replace('-',
                                                    '',
                                                    ) for record in records]
    # generate a number that represents a firstname in the set
    firstname_index = lambda: random.randrange(0,
                                               len(firstnames),
                                               )

    # generate a random birthdate range 1959 to 2000
    birthdate = lambda: datetime.datetime.fromtimestamp(random.randrange(-628473600,
                                                                         949363200,
                                                                         )
                                                        ).strftime('%d%m%Y')

    # generate unique NINs
    nin = [birthdate() + '{:05d}'.format(count) for count in range(0,
                                                                   len(records),
                                                                   )]
    # map fieldname to fieldnumber
    field = lambda x: config.USU_FIELDS.index(x)

    for count, record in enumerate(records):
        firstname = firstnames[firstname_index()]
        lastname = firstnames[firstname_index()] + 'sen'

        record_out = list(record)

        record_out[field('FODSELSNR')] = nin[count][:4] + nin[count][6:]
        record_out[field('FODSELSDATO')] = nin[count][:8]
        record_out[field('FORNAVN_ETTTERNAVN')] = firstname + ' ' + lastname
        record_out[field('ETTERNAVN_FORNAVN')] = lastname + ' ' + firstname
        record_out[field('FORNAVN')] = firstname
        record_out[field('ETTERNAVN')] = lastname
        record_out[field('ADRESSE')] = 'ADRESSE'
        record_out[field('ADRESSELINJE_2')] = 'ADRESSELINJE_2'
        record_out[field('POSTNR')] = '{:04d}'.format(random.randrange(0, 9999))
        record_out[field('POSTSTED')] = '{:04d}'.format(random.randrange(0, 9999))
        record_out[field('PERM_KODE')] = 0
        record_out[field('BANK_KONTO')] = 1234567890
        record_out[field('ANSATTNUMMER')] = '{:05d}'.format(count)
        record_out[field('BRUKERNAVN')] = generate_username(firstname)
        record_out[field('MEDARB_SAMTALE_MERKNAD')] = 'MEDARB_SAMTALE_MERKNAD'
        record_out[field('EPOSTADR')] = record_out[field('BRUKERNAVN')].lower() + '@fakedomain.ntnu.no'
        record_out[field('INTERN_TELEFON')] = '555' + str(random.randrange(10000,99999))
        record_out[field('MOBIL_TELEFONNR')] = '555' + str(random.randrange(10000,99999))
        record_out[field('STILLINGSBESKRIVELSE')] = 'STILLINGSBESKRIVELSE'
        record_out[field('PROSJEKT')] = 'PROSJEKT'
        record_out[field('STILL_BETEGNELSE')] = 'STILL_BETEGNELSE'
        record_out[field('ANALYSE')] = 'ANALYSE'
        record_out[field('BREG_1107')] = record_out[field('BRUKERNAVN')].lower() + '@fakedomain.hotmail.no'

        print('Writing line {:5d}'.format(count))

        write_record(record_out)


if __name__ == '__main__':
    main()

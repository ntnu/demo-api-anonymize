#!/usr/bin/env python3

# Author : Peter Davidse <peter.davidse@ntnu.no>
# Date   : 10.04.2017
#        : 13.02.2018 modified for use as anonymizer
# Info   : Configuration file used for ntnu_paga script

import os

# set configuration context, when run from shell, env variable overrides this!
DEFAULT_CONFIG = 'development'

# application root or base path (no trailing slash)
APP_BASE_PATH = os.path.abspath(os.path.dirname(__file__))

configurations = {
    'development': 'DevelopmentConfig',
    'production': 'ProductionConfig',
}


def configuration():
    configuration_name = os.getenv('NTNU_CONFIG',
                                   DEFAULT_CONFIG,
                                   )

    return eval(configurations[configuration_name])


class Config(object):

    # application root or base path
    APP_BASE_PATH = os.path.abspath(os.path.dirname(__file__)) + '/'

    ENCODING = 'iso-8859-1'

    OUTFILE_DELIMITER = ';'
    OUTFILE_QUOTE = 'csv.QUOTE_MINIMAL'
    OUTFILE_WRITEHEADER = False

    # Namedtuple for easy access to USU file fields
    USU_FIELDS = ['FODSELSNR',
                  'FODSELSDATO',
                  'ALDER',
                  'FORNAVN_ETTTERNAVN',
                  'ETTERNAVN_FORNAVN',
                  'ADRESSE',
                  'ADRESSELINJE_2',
                  'POSTNR',
                  'POSTSTED',
                  'KJONN',
                  'UTD_KODE',
                  'UTDANNING',
                  'TJEN_STEDNR',
                  'TJEN_STEDNAVN',
                  'TJEN_STEDKORTNAVN',
                  'ANSV_STEDNR',
                  'ANSV_STEDNAVN',
                  'INTERN_TELEFON',
                  'MOBIL_TELEFONNR',
                  'BRUKERNAVN',
                  'EPOSTADR',
                  'MACONOMYNR',
                  'KOSTN_STED_KODE',
                  'KOSTN_STED_NAVN',
                  'ANALYSE',
                  'ART',
                  'PROSJEKT',
                  'TJEN_FORHOLD_NR',
                  'ANT_TJEN_FORHOLD',
                  'STILL_KODE',
                  'STILL_BETEGNELSE',
                  'LONNSPLAN',
                  'LONNSRAMME',
                  'LTR_A',
                  'LTR_B',
                  'STILL_ANDEL',
                  'TJEN_ANS',
                  'ANS_I_AR',
                  'TJEN_FORHOLD',
                  'F_LONNSDAG',
                  'S_LONNSDAG',
                  'PERM_KODE',
                  'DBH_NR',
                  'DBH_STILL_KAT_NAVN',
                  'UNIV_KAT',
                  'NTNU_KAT',
                  'C_TAB',
                  'A_TAB_50',
                  'A_TAB_100',
                  'B_TAB_50',
                  'B_TAB_100',
                  'NATT_45',
                  'LONNS_KODE',
                  'AKSJ_KODE',
                  'AKSJ_DATO',
                  'NESTE_OPPR',
                  'NYTT_LTR_A',
                  'SK_FORM',
                  'SK_TAB',
                  'SK_PROS',
                  'SK_FRIBELOP',
                  'BANK_KONTO',
                  'RAPP_DATO',
                  'FAKULTET_AVDELING',
                  'INSTITUTT_SEKSJON',
                  'BTO_MND_LONN',
                  'BTO_DAG_LONN',
                  'BTO_DAG_LONN16',
                  'SKATTEKOMMUNENR',
                  'STATUS_RESERV_NETTPUBL',
                  'STILLINGSBESKRIVELSE',
                  'ETTERNAVN',
                  'FORNAVN',
                  'STATUS_HOVEDARBEIDSFORHOLD',
                  'ANSATTNUMMER',
                  'START_SISTE_STILL_OPPLYSN',
                  'START_NAVERENDE_ARBFORHOLD',
                  'FORBRUKSKODE',
                  'FRADATO',
                  'TILDATO',
                  'FORBRUKSANDEL',
                  'FO_TERMIN_VERDI',
                  'FO_TERMIN_FRADATO',
                  'FO_TERMIN_TILDATO',
                  'FO_TERMIN_MERKNAD',
                  'MEDARB_SAMTALE_DATO',
                  'MEDARB_SAMTALE_MERKNAD',
                  'BREG_1100',
                  'BREG_1101',
                  'BREG_1102',
                  'BREG_1103',
                  'BREG_1104',
                  'BREG_1105',
                  'BREG_1106',
                  'BREG_1107',]  # <- added, failed to map!



class ProductionConfig(Config):

    # location for files that needs to be analyzed/processed
    EXTERNAL_FILES_DIR = '/export/transit/paga/tia/'


class DevelopmentConfig(Config):

    EXTERNAL_FILES_DIR = APP_BASE_PATH + '/extfiles/'

